package com.example.marc.hardballpro;

import android.location.Location;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Vector;

/**
 * Created by Marc L. Madsen on 22-05-2017.
 */

public class Logger {

    File file;
    FileWriter fos;
    BufferedWriter bw;

    public Logger(){
        try {
            file = new File(Environment.getExternalStorageDirectory(), "location.csv");
        }
        catch (Exception e){
            Log.d("Exception: ", e.toString());
        }
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }


    public void write(Location lastLoc, Vector<Float> velocity, Vector<Float> predictionVector){

        if(lastLoc == null){
            return;
        }

        if(isExternalStorageWritable()){
            try {
                Log.d("Data", "wrote some stuff");
                fos = new FileWriter(file, true);
                bw = new BufferedWriter(fos);
                bw.append(System.currentTimeMillis() + ", " + lastLoc.getLatitude() + ", " + lastLoc.getLongitude() + ", " + lastLoc.getTime() + ", " + velocity.toString() + ", " + predictionVector.toString());
                bw.newLine();
                bw.flush();
                bw.close();
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
