package com.example.marc.hardballpro;

import android.content.Context;
import android.location.Location;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Team team;
    private WifiManager wm;
    private Map<String, Marker> markerMap = new HashMap<String, Marker>();
    private Marker myMarker = null;
    private String ip;
    Logger log = new Logger();

    //TODO Implementer opdateret billede af paintball bane.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //WifiManager to get our IP Address
        wm = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress()).split(":")[0];

        //Find out which team we picked -- need to know for which to display
        team = (Team) getIntent().getSerializableExtra("team_name");

        //Initialize coordinator
        Coordinator coord = new Coordinator(this);
        coord.execute(null, null, null);
    }

    public void updatePlayerPositions(ArrayList<Identifier> idfs) {
        for(Identifier idf: idfs) {
            updatePlayerPosition(idf);
        }
    }

    //Called when found new player location
    public void updatePlayerPosition(final Identifier idf){
        //Same team as ours
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable(){
            @Override
            public void run() {
                if(idf.getTeam() == team){
                    //Remove our current marker if it already exists
                    if (markerMap.containsKey(idf.getLocalIP())){
                        markerMap.get(idf.getLocalIP()).remove();
                    }
                    Marker marker;
                    if(idf.getTime() == 0) {
                        marker = mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(idf.getLatitude(), idf.getLongitude()))
                                .title("Teammate")
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.markerpng)));

                    } else {
                        double newLongitude = idf.getLongitude();
                        newLongitude = newLongitude + ((idf.getVelocity().get(0) * idf.getTime()) + (idf.getPredictionVector().get(0) * Math.pow(idf.getTime(), 2))) * 0.000001f;
                        double newLatitude = idf.getLatitude();
                        newLatitude = newLatitude + ((idf.getVelocity().get(1) * idf.getTime()) + (idf.getPredictionVector().get(1) * Math.pow(idf.getTime(), 2))) * 0.000001f;

                        marker = mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(newLatitude, newLongitude))
                                .title("Teammate")
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.markerpng)));
                    }
                    markerMap.put(idf.getLocalIP(), marker);
                }



            }
        });

    }

    public void updateOwnPosition(final Identifier myIdf){
        if(myIdf == null) {
            return;
        }
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable(){
            @Override
            public void run() {
                if(myMarker != null){
                    myMarker.remove();
                }
                if(myIdf.getTime() == 0) {
                    myMarker = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(myIdf.getLatitude(), myIdf.getLongitude()))
                            .title("You")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.markeryourpng)));
                } else {
                    double newLongitude = myIdf.getLongitude();
                    newLongitude = newLongitude + ((myIdf.getVelocity().get(0) * myIdf.getTime()) + (myIdf.getPredictionVector().get(0) * Math.pow(myIdf.getTime(), 2))) * 0.00001f;
                    double newLatitude = myIdf.getLatitude();
                    newLatitude = newLatitude + ((myIdf.getVelocity().get(1) * myIdf.getTime()) + (myIdf.getPredictionVector().get(1) * Math.pow(myIdf.getTime(), 2))) * 0.00001f;

                    myMarker = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(newLatitude, newLongitude))
                            .title("You")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.markeryourpng)));
                    
                }
            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(MAP_TYPE_SATELLITE);
        //Update location of ourselves
        CameraUpdate center=
                CameraUpdateFactory.newLatLng(new LatLng(56.1660291,10.123432));
        CameraUpdate zoom=CameraUpdateFactory.zoomTo(18.9F);

        mMap.moveCamera(center);
        mMap.animateCamera(zoom);

        LatLng NEWARK = new LatLng(56.1660291, 10.123232);

        GroundOverlayOptions newarkMap = new GroundOverlayOptions()
                .image(BitmapDescriptorFactory.fromResource(R.drawable.hardballmapnew))
                .position(NEWARK, 130f, 75f);
        mMap.addGroundOverlay(newarkMap);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);

    }

    public Team getTeam() {
        return team;
    }

    public String getIp() {
        return ip;
    }
}
