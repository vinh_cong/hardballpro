package com.example.marc.hardballpro;

import java.io.Serializable;
import java.util.Vector;

/**
 * Created by V on 01-05-2017.
 */

public class Identifier implements Serializable{

    private Team team;
    private String localIP;
    private double longitude;
    private double latitude;
    private Vector<Float> velocity;
    private Vector<Float> predictionVector;
    private long time = 0;

    public Identifier(Team team, Double longitude, Double latitude, String localIP, Vector<Float> velocity, Vector<Float> predictionVector) {
        this.team = team;
        this.localIP = localIP;
        this.latitude = latitude;
        this.longitude = longitude;
        this.velocity = velocity;
        this.predictionVector = predictionVector;
    }

    public Team getTeam() {
        return team;
    }

    public String getLocalIP() {
        return localIP;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public Vector<Float> getPredictionVector() {
        return predictionVector;
    }

    public Vector<Float> getVelocity() {
        return velocity;
    }

    public long getTime() {
        return time;
    }

    public void incrementTime() {
        time++;
    }
}
