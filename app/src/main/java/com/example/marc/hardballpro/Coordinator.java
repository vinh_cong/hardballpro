package com.example.marc.hardballpro;

import android.net.wifi.WifiManager;

import android.os.AsyncTask;
import android.util.Log;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

/**
 * Created by Marc on 10-04-2017.
 */

public class Coordinator extends AsyncTask<Void, Void, Void> {

    //EDIT THE INITIAL CONNECTION POINT HERE
    String initialIP = "192.168.43.91";

    LocationModule loc;
    MapActivity map;
    WifiManager wm;
    String ip;
    ThreadHandler th;
    ArrayList<Identifier> listOfIdentifiers = new ArrayList<>();

    public Coordinator(MapActivity map) {

        loc = new LocationModule(map.getApplicationContext(), map, this);


        this.map = map;
        ip = map.getIp();
        Log.d("Coordinator", "" + ip);

    }

    public void createConnection(final String address) {
        if(address.equals(ip)) {
            Log.d("Coordinator", "Aborting connection to: " + address + ", this is MY OWN IP");
            return;
        }
        try {
            Log.d("Coordinator", "attempting connection");
            Socket clientSocket = new Socket(address, 8888);
            Log.d("Coordinator", "Connection established");
            ConnectionThread ct = new ConnectionThread(clientSocket, th);
            th.addConnectionThread(ct);
            ct.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void broadcast(Object o, Object j, Object i, Object z) throws Exception {
        Identifier identifier;
        Object object = o;

        if (o instanceof Double && j instanceof Double) {
            identifier = new Identifier(map.getTeam(), (Double) o, (Double) j, map.getIp(), (Vector<Float>) i, (Vector<Float>) z);
            object = identifier;
            Log.d("Coordinator", "Broadcast: Identifier values are: TEAM: " + identifier.getTeam() + ", Location: " + o + ", " + j + ", MyIP: " + identifier.getLocalIP() +
                    ", with velocityVector: " + identifier.getVelocity() + ", and predictionVector: " + identifier.getPredictionVector());
        } else {
            Log.d("Coordinator", "Broadcasting new entry: " + o.toString());
        }
        th.broadcastToAllThreads(object);
    }

    public void setupServer() throws Exception {
        th = new ThreadHandler(this);
        th.start();
    }

    public void receiveFromThreads(Object o) {
        // Do something with identifiers
        if (o instanceof Identifier) {
            Identifier idf = (Identifier)o;
            Log.d("Coordinator", "Received identifier with values: TEAM: " + idf.getTeam() + ", Location: " + idf.getLongitude() + ", " + idf.getLatitude() +
                    ", IP: " + idf.getLocalIP() + ", velocity: " + idf.getVelocity() + ", predictionVector: " + idf.getPredictionVector());
            updateIdentifierList(idf);
            map.updatePlayerPosition((Identifier) o);

        } else if (o instanceof String) {
            boolean alreadyExist = false;
            for (ConnectionThread ct : th.getConnectionThreads()) {
                if(o.equals(ct.getIP())) {
                    alreadyExist = true;
                    Log.d("Coordinator", "Aborting new connection to: " + o.toString() + ", already exists!");
                }
            }
            if (!alreadyExist) {
                createConnection((String) o);
            }
        }
    }

    public void updateIdentifierList(Identifier idf) {

        if (listOfIdentifiers.isEmpty()) {
            listOfIdentifiers.add(idf);
        }

        Iterator<Identifier> iter = listOfIdentifiers.iterator();

        while (iter.hasNext()) {
            Identifier i = iter.next();
            if (idf.getLocalIP().equals(i.getLocalIP())) {
                listOfIdentifiers.remove(i);
                listOfIdentifiers.add(idf);
                listOfIdentifiers.trimToSize();
            } else {
                Log.d("IDFList", "else called");
                listOfIdentifiers.add(idf);
            }
        }

        Log.d("Size of idflist", ""+ listOfIdentifiers.size());
    }

    private void incrementIdentifierList() {
        if(listOfIdentifiers.isEmpty()) {
            return;
        }
        for(Identifier i : listOfIdentifiers) {
            i.incrementTime();
        }
    }

    private void initializeIncrementor() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    try {
                        Log.d("Incrementor Running", "");
                        map.updatePlayerPositions(listOfIdentifiers);
                        map.updateOwnPosition(loc.getMyIdentifier());
                        Thread.sleep(1000);
                        incrementIdentifierList();
                        loc.incrementMyIdentifier();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
        t.start();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            setupServer();
            createConnection(initialIP);
            initializeIncrementor();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return null;
        }
    }
}
