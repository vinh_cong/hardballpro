package com.example.marc.hardballpro;

import android.util.Log;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by V on 26-04-2017.
 */

public class ConnectionThread extends Thread {

    private Socket socket;
    private ObjectOutput oos;
    private ObjectInputStream ois;
    private ThreadHandler th;
    private String ip;

    public ConnectionThread(Socket socket, ThreadHandler th) {
        this.th = th;
        this.socket = socket;
    }
    public void run() {
        try {
            Log.d("ConnectionThread:", "remote address: " + socket.getRemoteSocketAddress());
            //Format to match
            ip = socket.getRemoteSocketAddress().toString().split(":")[0].substring(1);
            oos = new ObjectOutputStream(socket.getOutputStream());
            ois = new ObjectInputStream(socket.getInputStream());
            startListening();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendObject(Object o) {
        try {
            if(oos == null) {
                oos = new ObjectOutputStream(socket.getOutputStream());
            }
            oos.writeObject(o);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void startListening() {
        while (!isInterrupted()) {
            try {
                if(ois == null) {
                    ois = new ObjectInputStream(socket.getInputStream());
                }
                Log.d("ConnectionThread", "listening for Objects");
                Object o = ois.readObject();
                th.returnFromThread(o);

            } catch (IOException e) {
                Thread.currentThread().interrupt();//preserve the message
                Log.d("ConnectionThread", "terminating thread cuz of missing link");
                th.removeConnectionThread(this);
                return;//Stop doing whatever I am doing and terminate
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getIP() {
        return ip;
    }
}
