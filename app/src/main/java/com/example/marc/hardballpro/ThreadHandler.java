package com.example.marc.hardballpro;

import android.util.Log;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by V on 25-04-2017.
 */

public class ThreadHandler extends Thread {
    public final Coordinator coordinator;
    private ServerSocket serverSocket;
    private ArrayList<ConnectionThread> connectionThreads = new ArrayList<ConnectionThread>();

    public ThreadHandler(Coordinator coord) throws Exception {
        serverSocket = new ServerSocket(8888);
        this.coordinator = coord;
    }

    public void run() {
        while (!isInterrupted()) { //You should handle interrupts in some way, so that the thread won't keep on forever if you exit the app.

            ConnectionThread thread = null;
            try {
                Socket newSocket = serverSocket.accept();
                thread = new ConnectionThread(newSocket, this);
                Log.d("ThreadHandling", "New client found at: " + newSocket.getRemoteSocketAddress());
                //Format such that i can pass it onwards
                String newClient = new String(newSocket.getRemoteSocketAddress().toString().split(":")[0].substring(1));
                boolean alreadyExist = false;
                for(ConnectionThread ct : connectionThreads) {
                    if(ct.getIP().equals(newClient)) {
                        alreadyExist = true;
                        Log.d("ThreadHandling", "New client already exists. Aborting new connection setup");
                    }
                }
                if(!alreadyExist) {
                    coordinator.broadcast(newClient, null, null, null);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            connectionThreads.add(thread);
            thread.start();
        }
    }

    public void returnFromThread(Object o) {
        coordinator.receiveFromThreads(o);
    }

    public void addConnectionThread(ConnectionThread ct) {
        connectionThreads.add(ct);
    }

    public void removeConnectionThread(ConnectionThread ct) {
        if(connectionThreads.contains(ct)) {
            connectionThreads.remove(ct);
            Log.d("ThreadHandler", "Succesfully removed: " + ct);
        }
    }

    public void broadcastToAllThreads(final Object o) {
        if(!connectionThreads.isEmpty()) {
            Log.d("ThreadHandler", "Invoking Broadcasting");
            for (final ConnectionThread connectionThread : connectionThreads) {
                //connectionThread.interrupt();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        connectionThread.sendObject(o);
                        return;
                    }
                }).start();
                //connectionThread.startListening();
            }
        } else {
            Log.d("ThreadHandler", "Empty list: " + connectionThreads.toString());
        }

    }

    public ArrayList<ConnectionThread> getConnectionThreads() {
        return connectionThreads;
    }
}
