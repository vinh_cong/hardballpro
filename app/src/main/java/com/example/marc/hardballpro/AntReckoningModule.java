package com.example.marc.hardballpro;

import java.util.HashMap;
import java.util.Map;

import static com.example.marc.hardballpro.AntReckoningUtil.antMap;
import static com.example.marc.hardballpro.AntReckoningUtil.antMapTest;

/**
 * Created by Marc L. Madsen on 11-05-2017.
 */

public class AntReckoningModule {
    private int longitudeGridSize = 31;
    private int latitudeGridSize = 23;

    private double maxLatitude = 56.166268;
    private double minLatitude = 56.165773;
    private double maxLongitude = 10.123824;
    private double minLongitude = 10.122637;

    private double latitudeCellSize = (maxLatitude - minLatitude) / latitudeGridSize;
    private double longitudeCellSize = (maxLongitude - minLongitude) / longitudeGridSize;


    public AntReckoningModule(){

        System.out.println("Lat cell: " + latitudeCellSize);
        System.out.println("Long cell: " + longitudeCellSize);
        //TODO Explore the algorithm for approximate position, what variables we need etc.
    }

    private int[] getNewApproximatePosition(){
        /*                   **** Algoritmen ****
         x_(t+δt) = x_t + v_t*δt + 0.5 ( α * 1/m * F_t + (1 - α) * a_t ) * δt^2

                       **** Betydning af variabler ****
        x_t = current position
        v_t = velocity of P
        δt  = frames elapsed since last update
        α   = Weight coef. acceleration versus forces
        m   = masse i gram (newton)
        F_t = sum of attraction forces by pheromones on P and other forces (e.g. gravity)
        a_t = acceleration af P

                    **** Distributed implementation ****
        Option 1) Alle regner ud for alle, således at når de snakker med peers tæt på dem, vil de kunne vide hvis andre spilleres "prediciton errors" blive for høje, og dermed sende en update.
        Option 2) Alle regner ud for dem selv, og udsender en "prediction vector" vdr. hvor de regner med at være. Når man ser at ens prediction error bliver for høj på en selv, sender man en update
                  ud vdr. dette.


        Option 2 virker som den bedste måde at gøre det på også ift. overhead.

        */

        return null;
    }

    //Takes a position (latitude, longitide) as input and gets its surrounding coordinates in the grid and their respective attraction values as defined in antMap.
    public Map<Double[], Integer> getAttractionLocations(double longitude, double latitude){
        Map<Double[], Integer> res = new HashMap<Double[], Integer>();

        // [0] = latitude, [1] = longitude
        int[] myPosInGrid = getMyPositionInGrid(longitude, latitude);

        //myPosInGrid[0] = latitude

        int i = 0;
        int j = 0;
        while(myPosInGrid[0] - 2 + i <= myPosInGrid[0] + 2){

            //See if values can be found within our grid, otherwise continue.
            if((myPosInGrid[0] - 2 + i) < 0 || myPosInGrid[1] - 2 < 0){
                i++;
                continue;
            }

            while((myPosInGrid[1] - 2) + j <= myPosInGrid[1] + 2) {

                //See if values can be found within our grid, otherwise continue.
                if(myPosInGrid[0] + 2 > antMap.length || myPosInGrid[1] + 2 > antMap.length){
                    j++;
                    continue;
                }

                int currentLatitudeGrid = myPosInGrid[0] - 2 + i;
                int currentLongtitudeGrid = myPosInGrid[1] - 2 + j;
                int attractionForce = antMap[currentLatitudeGrid][currentLongtitudeGrid];

                if(attractionForce != 0 && attractionForce != AntReckoningUtil.x){
                    double currentLatitude = latitude + -1*(currentLatitudeGrid - myPosInGrid[0]) * latitudeCellSize;
                    double currentLongitude = longitude + (currentLongtitudeGrid - myPosInGrid[1]) * longitudeCellSize;

                    /*
                    System.out.println("Pos in grid: (" + currentLongtitudeGrid + ", " + currentLatitudeGrid
                            + ") Lat movement " + -1*(currentLatitudeGrid - myPosInGrid[0])
                            + ", Long movement " + (currentLongtitudeGrid - myPosInGrid[1])
                            + " attrac: " + attractionForce);

                    */

                    res.put(new Double[]{currentLongitude, currentLatitude}, attractionForce);
                }

                j++;
            }
            i++;
            j = 0;
        }

        return res;
    }

    public int[] getMyPositionInGrid(double longitude, double latitude){
        //TODO Check when out of map

        int gridLatitude  = (int) Math.floor(  (maxLatitude - latitude)  / latitudeCellSize  );
        int gridLongitude = longitudeGridSize - (int) Math.floor( (maxLongitude - longitude) / longitudeCellSize );

        System.out.println("Current position in GRID: " + gridLongitude + ", " + gridLatitude);

        return new int[]{gridLatitude, gridLongitude};
    }


}
