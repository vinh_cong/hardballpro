package com.example.marc.hardballpro;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

public class GUIActivity extends AppCompatActivity {

    //      TODO *NEEDS*
    // TODO Add simple GUI with which team to join and countdown. Initiate new intent for map when done.
    // TODO Some kind of callback from the Coordinator whenever a new location is found to process it.


    //      TODO **IDEAS**
    // Synchronized countdown
    // Cheat prevention (blåt hold kan kun se blåt hold)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gui);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                startActivity(intent);
            }
        });

        final Button red_team_button = (Button) findViewById(R.id.red_team_button);
        red_team_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Team team = Team.RED;
                Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                intent.putExtra("team_name", team);
                startActivity(intent);
            }
        });


        final Button blue_team_button = (Button) findViewById(R.id.blue_team_button);
        blue_team_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Team team = Team.BLUE;
                Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                intent.putExtra("team_name", team);
                startActivity(intent);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_gui, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automaticlly handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
