package com.example.marc.hardballpro;

import java.util.Vector;

/**
 * Created by Marc L. Madsen on 12-05-2017.
 */


public class VectorUtil {

    public static Vector<Float> add(Vector<Float> a, Vector<Float> b){
        if(a.size() != b.size()) throw new IllegalArgumentException("Vectors must be of the same size.");
        Vector<Float> res = new Vector<>(a.size());
        for (int i = 0; i < a.size(); i++) {
            res.add(i, a.get(i) + b.get(i));
        }
        return res;
    }

    public static Vector<Float> scale(float factor, Vector<Float> a){
        Vector<Float> res = new Vector<>();
        for (int i = 0; i < a.size(); i++) {
            res.add(i, factor * a.get(i));
        }
        return res;
    }

    public static double length(Vector<Float> a){
        double sum = 0;
        for(float f: a){
            sum += f*f;
        }
        return Math.sqrt(sum);
    }
}


