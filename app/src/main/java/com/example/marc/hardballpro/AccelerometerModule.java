package com.example.marc.hardballpro;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.Vector;

/**
 * Created by Marc L. Madsen on 12-05-2017.
 */

public class AccelerometerModule implements SensorEventListener {
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private Vector<Float> currentAcceleration;

    public AccelerometerModule(Context context) {
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if(sensorEvent.sensor.getType() == (Sensor.TYPE_LINEAR_ACCELERATION)) {
            Vector<Float> tempAccel = new Vector<>();
            tempAccel.add(sensorEvent.values[0]);
            tempAccel.add(sensorEvent.values[1]);
            tempAccel.add(sensorEvent.values[2]);

            currentAcceleration = tempAccel;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        //Do nothing
    }

    public Vector<Float> getCurrentAcceleration() {
        return currentAcceleration;
    }
}
