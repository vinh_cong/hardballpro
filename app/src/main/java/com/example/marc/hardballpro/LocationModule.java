package com.example.marc.hardballpro;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;

import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.Map;
import java.util.Random;
import java.util.Vector;

/**
 * Created by Marc on 10-04-2017.
 */

public class LocationModule implements LocationListener {

    private LocationManager locationManager;
    private Context context;
    private String provider = LocationManager.GPS_PROVIDER;
    private Location location;
    private long time;
    private Location lastLocation;
    private long lastTime;
    private Vector<Float> velocity;
    private Vector<Float> oldVelocity;
    private MapActivity mapActivity;
    private Coordinator coord;
    private AccelerometerModule accelerometer;
    private Vector<Float> lastAcceleration;
    private Vector<Float> acceleration;
    private Vector<Float> predictionVector;
    private Identifier myIdentifier;

    // KOEFFICIENTS FOR SCALING PURPOSES
    private float accelerationCoef = 0.8f;
    private float velocityCoef = 0.8f;

    private AntReckoningModule antReckoningModule;

    //TODO Add new type of markers for blue and red team.

    //TODO Section 4 + Section 5

    //              **** MISC ****
    //          **** ANT RECKONING ****
    //TODO Implement AntReckoning:
    //TODO DeadReckoning: A Vector that determines where the user is moving.
    //TODO Attractiveness: other players, game objects, specific locations

    public LocationModule(Context context, MapActivity mapActivity, Coordinator coord){
        this.context = context;
        this.mapActivity = mapActivity;
        this.coord = coord;
        this.accelerometer = new AccelerometerModule(mapActivity.getApplicationContext());
        this.antReckoningModule = new AntReckoningModule();

        // Acquire a reference to the system Location Manager
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("LOCATION","LocationModule couldn't be activated");
        }
        else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, this);
            Log.d("LOCATION","LocationModule activated");
        }

        velocity = new Vector<Float>();
        velocity.add(0f);
        velocity.add(0f);
    }


    public Vector<Float> getPredictionVector(){

        //TODO Fix så den bruger rigtig location
        //Fake Location -- "LATITUDE ER DEN STORE"
        /*
        Location fakeLocation = new Location("");
        fakeLocation.setLongitude(10.12292);
        fakeLocation.setLatitude(56.166143);
        */

        Map<Double[], Integer> attractionMap = antReckoningModule.getAttractionLocations(location.getLongitude(), location.getLatitude());
        Vector<Float> res = new Vector<>();
        res.add(0f);
        res.add(0f);

        //Iterate over set of surrounding locations and their attraction values, to generate one attraction vector..
        for (Map.Entry<Double[], Integer> entry : attractionMap.entrySet())
        {
            Location l = new Location("");
            l.setLongitude(entry.getKey()[0]);
            l.setLatitude(entry.getKey()[1]);

            //Get vector between locations
            Vector v = vectorFromLocation(location, l);
            int attractionForce = entry.getValue();

            // System.out.println(v.toString());

            //Scale vector up accordingly
            Vector<Float> temp = new Vector<>();
            for (int i = 0; i < v.size(); i++) {
                temp.add(i, (attractionForce * (float) v.get(i) * 0.000039f * 0.5f));
            }

            if(res == null){
                res = temp;
            }
            else res = VectorUtil.add(res, temp);

        }

        System.out.println("Final AttractionVector: " + res.toString());

        return res;
    }

    Logger log = new Logger();
    @Override
    public void onLocationChanged(final Location newLocation) {

        if(location != null) {
            lastAcceleration = acceleration;
            lastLocation = location;
            lastTime = time;
        }

        acceleration = accelerometer.getCurrentAcceleration();
        this.location = newLocation;

        //Time calculation
        time = System.currentTimeMillis();

        if(location != null && lastLocation != null) {
            calculateVelocity(lastLocation, location, lastTime, time);
            calculateAcceleration(lastTime, time);
        }

       predictionVector = getPredictionVector();
        Log.d("AttractionVector", "" + predictionVector);

        if (location != null) {
            Log.d("onLocationChanged", location.toString());
            initializeMyIdentifier();
            mapActivity.updateOwnPosition(myIdentifier);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        coord.broadcast(location.getLongitude(), location.getLatitude(), velocity, predictionVector);
                        log.write(lastLocation, velocity, predictionVector);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        return;
                    }
                }
            }).start();

        }
    }

    //EMA estimation of acceleration
    public void calculateAcceleration(long oldTime, long newTime) {

        float timeDifference = newTime - oldTime;

        //timeDifference in seconds
        timeDifference = timeDifference / 1000;

        //Acceleration calculation is going to be in meters/second
        float f = 1f/timeDifference;
        acceleration = VectorUtil.scale(f, acceleration);

        //Apply coefficient
        acceleration = VectorUtil.scale(accelerationCoef, acceleration);

        if(lastAcceleration != null) {
            Log.d("Accelerations", "old is: " + lastAcceleration + ", and new is: " + acceleration);
            lastAcceleration = VectorUtil.scale(1f - accelerationCoef, lastAcceleration);
            acceleration = VectorUtil.add(acceleration, lastAcceleration);
        }
        Log.d("Accel Calculation", "Acceleration in units/second is: " + acceleration);

    }

    //EMA estimation of velocities
    public void calculateVelocity(Location oldLoc, Location newLoc, long oldTime, long newTime) {
        if(velocity != null) {
            oldVelocity = velocity;
        }
        float timeDifference = newTime - oldTime;

        //timeDifference in seconds
        timeDifference = timeDifference / 1000;

        //Vector from mock positions
        velocity = vectorFromLocation(oldLoc, newLoc);

        //Velocity calculation is going to be in meters/second
        float f = 1f/timeDifference;
        velocity = VectorUtil.scale(f, velocity);

        //Apply coefficient
        velocity = VectorUtil.scale(velocityCoef, velocity);

        //Margin for "standing still"
        //if(velocity.get(0) < 2f && velocity.get(0) < 2f) {
        //  velocity.set(0, 0f);
        //    velocity.set(1, 0f);
        //}

        //Add previous estimation
        if(oldVelocity != null) {
            Log.d("Velocities", "old is: " + oldVelocity + ", and new is: " + velocity);
            oldVelocity = VectorUtil.scale(1f - velocityCoef, oldVelocity);
            velocity = VectorUtil.add(velocity, oldVelocity);
        }


        Log.d("Velocity Calculation", "Velocity in units/second is: " + velocity);
    }


    public Vector<Float> vectorFromLocation(Location l1, Location l2) {
        //Mock Positions to create 2D vector
        Location mockXold = new Location("");
        mockXold.setLongitude(l1.getLongitude());
        mockXold.setLatitude(0);

        Location mockXnew = new Location("");
        mockXnew.setLongitude(l2.getLongitude());
        mockXnew.setLatitude(0);

        Location mockYold = new Location("");
        mockYold.setLongitude(0);
        mockYold.setLatitude(l1.getLatitude());

        Location mockYnew = new Location("");
        mockYnew.setLongitude(0);
        mockYnew.setLatitude(l2.getLatitude());

        Vector<Float> result = new Vector<Float>();

        if(mockXnew.getLongitude() < mockXold.getLongitude()){
            result.add(-1 * mockXold.distanceTo(mockXnew));
        }
        else result.add(mockXold.distanceTo(mockXnew));

        if(mockYnew.getLatitude() < mockYold.getLatitude()){
            result.add(-1 * mockYold.distanceTo(mockYnew));
        }
        else result.add(mockYold.distanceTo(mockYnew));

        return result;
    }

    public void initializeMyIdentifier() {
        myIdentifier = new Identifier(mapActivity.getTeam(), location.getLongitude(), location.getLatitude(), mapActivity.getIp(), velocity, predictionVector);
        Log.d("InitializeMyIDF", "MyIdentifier values are: TEAM: " + myIdentifier.getTeam() + ", Location: " + myIdentifier.getLongitude() + ", " + myIdentifier.getLatitude() + ", MyIP: " + myIdentifier.getLocalIP() +
                ", with velocityVector: " + myIdentifier.getVelocity() + ", and predictionVector: " + myIdentifier.getPredictionVector());
    }

    public void incrementMyIdentifier() {
        if(myIdentifier == null) {
            return;
        }
        myIdentifier.incrementTime();
    }

    public Identifier getMyIdentifier() {
        return myIdentifier;
    }

    public Location getLastLocation(){
        return location;
    }

    public double getLongtitude(){
        return location.getLongitude();
    }

    public double getLatitude(){
        return location.getLatitude();
    }

    public double getRndLatitude(){
        Random r = new Random();
        return 56.168952 + (56.1721741-56.168952) * r.nextDouble();
    }

    public double getRndLongtitude(){
        Random r = new Random();
        return 10.185923 + (10.1902683-10.185923) * r.nextDouble();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("onProviderEnabled", "Provider enabled: " + provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("onProviderDisabled", "Provider enabled: " + provider);
    }


    private void message(String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }
}
